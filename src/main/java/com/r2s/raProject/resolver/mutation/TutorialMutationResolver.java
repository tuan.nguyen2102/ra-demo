package com.r2s.raProject.resolver.mutation;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.r2s.raProject.data.entity.Author;
import com.r2s.raProject.data.entity.Tutorial;
import com.r2s.raProject.data.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.r2s.raProject.data.repository.TutorialRepository;
import javassist.NotFoundException;

import java.util.Optional;

@Component
public class TutorialMutationResolver implements GraphQLMutationResolver {

    @Autowired
    private TutorialRepository tutorialRepository;
    @Autowired
    private AuthorRepository authorRepository;

    public Tutorial createTutorial(String title, String description, Integer authorId) {
        Tutorial book = new Tutorial();
        book.setAuthor(authorRepository.findById(authorId).get());
        book.setTitle(title);
        book.setDescription(description);

        tutorialRepository.save(book);

        return book;
    }

    public boolean deleteTutorial(Integer id) {
        tutorialRepository.deleteById(id);
        return true;
    }

    public Tutorial updateTutorial(Integer id, String title, String description) throws NotFoundException {
        Optional<Tutorial> optTutorial = tutorialRepository.findById(id);

        if (optTutorial.isPresent()) {
            Tutorial tutorial = optTutorial.get();

            if (title != null)
                tutorial.setTitle(title);
            if (description != null)
                tutorial.setDescription(description);

            tutorialRepository.save(tutorial);
            return tutorial;
        }

        throw new NotFoundException("Not found Tutorial to update!");
    }
}
