package com.r2s.raProject.resolver.mutation;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.r2s.raProject.data.dto.AuthorDTO;
import com.r2s.raProject.data.entity.Author;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.r2s.raProject.data.repository.AuthorRepository;

@Component
public class AuthorMutationResolver implements GraphQLMutationResolver {

    @Autowired
    private AuthorRepository authorRepository;

    public Author createAuthor(AuthorDTO authorDTO) {
        Author author = new Author();
        author.setName(authorDTO.getName());
        author.setAge(authorDTO.getAge());
        authorRepository.save(author);
        return author;
    }

    public Author updateAuthor(AuthorDTO authorDTO, Integer id){
        Author author = authorRepository.findById(id).get();
        author.setName(authorDTO.getName());
        author.setAge(authorDTO.getAge());
        authorRepository.save(author);
        return author;
    }

    public Boolean deleteAuthor(Integer id){
        authorRepository.deleteById(id);
        return true;
    }

}
